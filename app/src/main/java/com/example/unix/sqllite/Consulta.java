package com.example.unix.sqllite;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Consulta extends Activity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_lista);

         ArrayList<String> cont = new ArrayList<>();

        Conexion sqlite = new Conexion( this );
        sqlite.abrir();


        Cursor cursor = sqlite.getProspectos();//Se obtiene registros
        cont = sqlite.imprimeProspectos(cursor);


        //Creamos un nuevo ArrayAdapter con nuestra lista de cosasPorHacer
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, cont);

        //Seleccionamos la lista de nuestro layout
        ListView miLista = (ListView) findViewById(R.id.listView);

        //Se agregan los titulos al listview
        miLista.setAdapter(arrayAdapter);

        //System.out.println("===========================> " + datos);


        sqlite.cerrar();
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //aqui muestra el contenido seleccionado
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
