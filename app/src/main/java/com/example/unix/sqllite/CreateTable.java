package com.example.unix.sqllite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CreateTable extends SQLiteOpenHelper {

    //nombre de la base de datos
    private static final String NAMEDATABASE = "prospectos";

    //versión de la base de datos
    private static final int VERSION = 1;

    //nombre tabla y campos de tabla
    public final String tabla = "prospectos";
    public final String id = "id";
    public final String nombre = "nombre";
    public final String apellido = "apellido";
    public final String apellidom = "apellidom";
    public final String telefono = "telefono";
    public final String email = "email";
    public final String observacion = "observacion";


    //Instrucción SQL para crear las tablas
    private final String sql = "CREATE TABLE " + tabla + " ( " + id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            + nombre + " TEXT, "
            + apellido + " TEXT, "
            + apellidom + " TEXT, "
            + telefono + " TEXT, "
            + email + " TEXT, "
            + observacion + " TEXT )";


    public CreateTable(Context context) {
        super( context, NAMEDATABASE, null, VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL( sql );
    }

    @Override
    public void onUpgrade( SQLiteDatabase db,  int oldVersion, int newVersion ) {
        if ( newVersion > oldVersion )
        {
            //elimina tabla
            db.execSQL( "DROP TABLE IF EXISTS " + tabla );
            //y luego creamos la nueva tabla
            db.execSQL( sql );
        }
    }

}