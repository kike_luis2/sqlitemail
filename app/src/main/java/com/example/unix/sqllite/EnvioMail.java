package com.example.unix.sqllite;


import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class EnvioMail extends AsyncTask {

    private ProgressDialog statusDialog;
    private Activity sendMailActivity;

    public EnvioMail(Activity activity) {
        sendMailActivity = activity;

    }

    protected void onPreExecute() {
        statusDialog = new ProgressDialog(sendMailActivity);
        statusDialog.setMessage("......");
        statusDialog.setIndeterminate(false);
        statusDialog.setCancelable(false);
        statusDialog.show();
    }

    @Override
    protected Object doInBackground(Object... args) {
        try {

            publishProgress("Espere.........");

            Configuracion androidEmail = new Configuracion(args[0].toString(),
                    args[1].toString(), (List) args[2], args[3].toString(),
                    args[4].toString(), (List) args[5]);

            publishProgress("Preparando email....");
            androidEmail.createEmailMessage();
            publishProgress("Eviando email....");
            androidEmail.sendEmail();
            publishProgress("Email Enviado.");

        } catch (Exception e) {
            publishProgress(e.getMessage());
        }
        return null;
    }

    @Override
    public void onProgressUpdate(Object... values) {
        statusDialog.setMessage(values[0].toString());

    }

    @Override
    public void onPostExecute(Object result) {
        statusDialog.dismiss();
    }

}
