package com.example.unix.sqllite;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class Exportar extends Activity {


    ArrayList<String> cont = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_exportar);


        Conexion sqlite = new Conexion( this );
        sqlite.abrir();


        Cursor cursor = sqlite.getProspectos();//Se obtiene registros
        cont = sqlite.imprimeProspectos(cursor);
        final Button send = (Button) this.findViewById(R.id.button5);

        send.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {


                String fromEmail = "usuariomail";

                String fromPassword = "password";

                String toEmails = ((TextView) findViewById(R.id.editText7))
                        .getText().toString();

                List<String> toEmailList = Arrays.asList(toEmails
                        .split("\\s*,\\s*"));



                String emailSubject = "SqLite Prospectos";

                String emailBody = "prueba";

                new EnvioMail(Exportar.this).execute(fromEmail,
                        fromPassword, toEmailList, emailSubject, emailBody,cont);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //aqui muestra el contenido seleccionado
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }




}
