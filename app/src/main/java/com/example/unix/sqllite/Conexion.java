package com.example.unix.sqllite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;

public class Conexion {

    CreateTable sqliteHelper;
    SQLiteDatabase db;

    ArrayList<String> dato = new ArrayList<String>();

    public ArrayList<String> getDatos(){
        return dato;
    }


    /** Constructor de clase */
    public Conexion(Context context)
    {
        sqliteHelper = new CreateTable( context );
    }

    /** Abre conexion a base de datos */
    public void abrir(){
        Log.i("Conexion", "Se abre conexion a la base de datos " + sqliteHelper.getDatabaseName() );
        db = sqliteHelper.getWritableDatabase();
    }

    /** Cierra conexion a la base de datos */
    public void cerrar()
    {
         sqliteHelper.close();
    }


    public Cursor getProspectos()
    {

        return db.query( sqliteHelper.tabla ,
                new String[]{ sqliteHelper.id ,
                        sqliteHelper.nombre ,
                        sqliteHelper.apellido,
                        sqliteHelper.apellidom,
                        sqliteHelper.telefono,
                        sqliteHelper.email,
                        sqliteHelper.observacion},null,
                null, null, null,
                null );
    }


    public boolean insertarProspecto( String nombre, String apellido, String apellidom, String telefono, String email, String observacion )
    {

        boolean inserto = false;

        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put(sqliteHelper.nombre, nombre);
            contentValues.put(sqliteHelper.apellido, apellido);
            contentValues.put(sqliteHelper.apellidom, apellidom);
            contentValues.put(sqliteHelper.telefono, telefono);
            contentValues.put(sqliteHelper.email, email);
            contentValues.put(sqliteHelper.observacion, observacion);
            db.insert(sqliteHelper.tabla, null, contentValues);

            inserto = true;
        }catch(SQLiteException e){
            inserto = false;
        }

        return inserto;
    }


    public int eliminarProspecto( int id ){
        return db.delete( sqliteHelper.tabla , sqliteHelper.id + " = " + id ,  null);
    }


    public int actualizarPersona( int id, String nombre, String apellido, String apellidom  ){
        ContentValues contentValues = new ContentValues();

        contentValues.put( sqliteHelper.nombre , nombre);
        contentValues.put( sqliteHelper.apellido , apellido);
        contentValues.put( sqliteHelper.apellidom , apellidom);
        return db.update( sqliteHelper.tabla , contentValues, sqliteHelper.id + " = " + id , null);
    }


    public  ArrayList<String> imprimeProspectos( Cursor cursor ){

        if( cursor.moveToFirst() )
        {
            do{

               dato.add(cursor.getString( 1 ));

            }while( cursor.moveToNext() );
        }
        return dato;
    }

}
