package com.example.unix.sqllite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;








public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void consultar(View v){

        Intent i = new Intent(this, com.example.unix.sqllite.Consulta.class);
        startActivity(i);

    }

    public void exportar(View v){

        Intent i = new Intent(this, com.example.unix.sqllite.Exportar.class);
        startActivity(i);

    }


    public void agregar(View v){

        Intent i = new Intent(this, com.example.unix.sqllite.AltaProspecto.class);
        startActivity(i);

    }




}
