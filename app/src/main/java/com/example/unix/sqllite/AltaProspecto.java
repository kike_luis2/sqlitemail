package com.example.unix.sqllite;


import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class AltaProspecto extends Activity{


    private EditText nombre,apellido,apellidom,telefono,email, observacion;
    SQLiteDatabase db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_alta);

       nombre =(EditText)findViewById(R.id.editText);
        apellido=(EditText)findViewById(R.id.editText2);
        apellidom = (EditText)findViewById(R.id.editText3);
        telefono=(EditText)findViewById(R.id.editText4);
        email=(EditText)findViewById(R.id.editText5);
        observacion= (EditText)findViewById(R.id.editText6);

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void agregarDato(View v){


        String nombre,apellido,apellidom,telefono,email,observacion;

        nombre=this.nombre.getText().toString();
        apellido=this.apellido.getText().toString();
        apellidom=this.apellidom.getText().toString();
        telefono=this.telefono.getText().toString();
        email=this.email.getText().toString();
        observacion = this.observacion.getText().toString();


        Conexion sqlite = new Conexion( this );
        sqlite.abrir();

        //inserta datos
       boolean status =  sqlite.insertarProspecto(nombre, apellido, apellidom, telefono, email, observacion);

        if(status == true){

            Toast.makeText(this, "Datos Guardados ", Toast.LENGTH_LONG).show();
            this.nombre.setText("");
            this.apellido.setText("");
            this.apellidom.setText("");
            this.telefono.setText("");
            this.email.setText("");
            this.observacion.setText("");

        }else {
            Toast.makeText(this, "Error al Guardar ", Toast.LENGTH_LONG).show();

        }


    }



}



